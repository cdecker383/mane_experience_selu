﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1;

namespace WebApplication1.Controllers
{
    public class Registered_EventsController : Controller
    {
        private ManeExpEntities db = new ManeExpEntities();

        // GET: Registered_Events
        public ActionResult Index()
        {
            return View(db.Registered_Events.ToList());
        }

        // GET: Registered_Events/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registered_Events registered_Events = db.Registered_Events.Find(id);
            if (registered_Events == null)
            {
                return HttpNotFound();
            }
            return View(registered_Events);
        }

        // GET: Registered_Events/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Registered_Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "rg_event_id,event_description,event_location,event_start_time,reg_id")] Registered_Events registered_Events)
        {
            if (ModelState.IsValid)
            {
                db.Registered_Events.Add(registered_Events);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(registered_Events);
        }

        // GET: Registered_Events/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registered_Events registered_Events = db.Registered_Events.Find(id);
            if (registered_Events == null)
            {
                return HttpNotFound();
            }
            return View(registered_Events);
        }

        // POST: Registered_Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "rg_event_id,event_description,event_location,event_start_time,reg_id")] Registered_Events registered_Events)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registered_Events).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(registered_Events);
        }

        // GET: Registered_Events/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registered_Events registered_Events = db.Registered_Events.Find(id);
            if (registered_Events == null)
            {
                return HttpNotFound();
            }
            return View(registered_Events);
        }

        // POST: Registered_Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Registered_Events registered_Events = db.Registered_Events.Find(id);
            db.Registered_Events.Remove(registered_Events);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
